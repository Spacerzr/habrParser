package ru.gazprom_inform.dgzorin.appgui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImageLogo extends JPanel {

	@Override
	public void paintComponent(Graphics g) {
		BufferedImage im = null;
		String path = "/gazprom_logo.png";
		im = load(path);
		g.drawImage(im, 0, 0, null);
	}

	public BufferedImage load(String path) {
		try {
			return ImageIO.read(loadIs((String) path));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static InputStream loadIs(String path) {
		InputStream input = ImageLogo.class.getResourceAsStream(path);
		if (input == null) {
			input = ImageLogo.class.getResourceAsStream("/" + path);
		}
		return input;
	}
}
