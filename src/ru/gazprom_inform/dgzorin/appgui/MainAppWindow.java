package ru.gazprom_inform.dgzorin.appgui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import ru.gazprom_inform.dgzorin.logic.Constants;
import ru.gazprom_inform.dgzorin.logic.Habrahabr;
import ru.gazprom_inform.dgzorin.logic.Logger;
import ru.gazprom_inform.dgzorin.logic.SiteParser;

public class MainAppWindow {
	public static final JTextArea log = new JTextArea(6, 25);
	private Logger logger = new Logger();
	private JFrame viewApp;

	private SiteParser parser = new SiteParser();
	private ArrayList<Habrahabr> mainGraph = new ArrayList<>();
	private final String onlyNumbers = "0123456789500";

	public static void main(String[] args) {
		SwingUtilities.invokeLater(MainAppWindow::new);
	}

	MainAppWindow() {
		initGUI();
	}

	private void initGUI() {

		viewApp = new JFrame();
		JPanel mainPanel = new JPanel(new GridLayout(3, 1));
		JPanel upPanel = new JPanel(new GridLayout(1, 2));
		JPanel upRightPanel = new JPanel(new FlowLayout());
		JPanel middlePanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new FlowLayout());

		JFileChooser fc = new JFileChooser();
		FileNameExtensionFilter filterFileFormat = new FileNameExtensionFilter("*.xlsx", "*.xls", "*.*");
		fc.setFileFilter(filterFileFormat);
		fc.setSelectedFile(new File("excel_report"));

		JLabel testTask = new JLabel("Тестовое задание");
		JLabel author = new JLabel("Автор : Зорин Дмитрий");
		JLabel authorPhone = new JLabel("+7 (926) 268 48 81");

		JLabel labelEnterLink = new JLabel("Укажите корневую статью графа :");
		JTextField tfEnterLink = new JTextField(20);
		JLabel labelEnterNumb = new JLabel("Укажите количество элементов графа :");
		final JTextField graphSize = new JTextField(4);

		JButton btnGenerateGraph = new JButton("Сформировать граф");
		JButton btnGenerateReport = new JButton("Сформировать Отчет");

		JScrollPane scrollPane = new JScrollPane(log, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		viewApp.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		viewApp.setSize(330, 350);
		viewApp.setLocationRelativeTo(null);
		viewApp.setTitle("Gazprom-Inform Test Task");
		viewApp.setResizable(false);

		log.setEditable(false);
		log.setLineWrap(true);
		log.setWrapStyleWord(true);
		log.append("Лог событий\n");
		tfEnterLink.setText(Constants.graphVertexLink);
		btnGenerateReport.setEnabled(false);

		setVertexNumberFilter(graphSize, onlyNumbers, 3);
		graphSize.setText("500");
		upRightPanel.add(testTask);
		upRightPanel.add(author);
		upRightPanel.add(authorPhone);
		upPanel.add(new ImageLogo());
		upPanel.add(upRightPanel);

		middlePanel.add(labelEnterLink);
		middlePanel.add(tfEnterLink);
		middlePanel.add(labelEnterNumb);
		middlePanel.add(graphSize);
		middlePanel.add(btnGenerateGraph);
		middlePanel.add(btnGenerateReport);

		downPanel.add(scrollPane);

		mainPanel.add(upPanel);
		mainPanel.add(middlePanel);
		mainPanel.add(downPanel);

		viewApp.add(mainPanel);
		viewApp.setVisible(true);

		btnGenerateGraph.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (parser.checkLink(tfEnterLink.getText())) {
					logger.sendMsgToLog("Ожидайте, идет построение графа...");
					btnGenerateGraph.setEnabled(false);
					parser.setGraphSize(Integer.parseInt(graphSize.getText()));

					new Thread(new Runnable() {
						@Override
						public void run() {
							mainGraph = parser.parseHabrahabr();
							// filter = new Filter(mainGraph);
							btnGenerateGraph.setEnabled(true);
							btnGenerateReport.setEnabled(true);
						}
					}).start();
				}
			}
		});

		btnGenerateReport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				new ReportWindow(mainGraph);
			}
		});
	}

	private void setVertexNumberFilter(JTextField field, String filter, int limit) {
		field.setDocument(new PlainDocument() {
			@Override
			public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
				if (filter.indexOf(str) != -1) {
					if (getLength() < limit) {
						super.insertString(offs, str, a);
					}
				}
			}
		});
	}
}
