package ru.gazprom_inform.dgzorin.appgui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import ru.gazprom_inform.dgzorin.logic.Constants;
import ru.gazprom_inform.dgzorin.logic.ExportReport;
import ru.gazprom_inform.dgzorin.logic.Filter;
import ru.gazprom_inform.dgzorin.logic.Habrahabr;

public class ReportWindow {
	private JFrame viewReport;
	private ArrayList<Habrahabr> mainGraph = new ArrayList<>();
	HashSet<String> selectedTags = new HashSet<>();
	private Filter filter;

	JTable reportTable;

	private JButton btnAddTagToFilter = new JButton("Добавить в фильтр");
	private JButton btnClearFilter = new JButton("Очистить в фильтр");
	private JButton btnGenerateByTags = new JButton("Отчет по тегам");
	private JButton btnGenerateByLinks = new JButton("Отчет по ссылкам");
	private JButton btnGenerateExcel = new JButton("Экспорт в Excel");

	public ReportWindow(ArrayList<Habrahabr> mainGraph) {
		this.mainGraph = mainGraph;
		initGUI();
	}

	private void initGUI() {
		viewReport = new JFrame("Отчет");

		viewReport.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		viewReport.setSize(1300, 760);
		viewReport.setLocationRelativeTo(null);
		viewReport.setResizable(false);
		viewReport.setLayout(new FlowLayout());

		TableModel model = new TableModel(mainGraph);
		reportTable = new JTable(model);
		reportTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		reportTable.getColumnModel().getColumn(0).setPreferredWidth(35);
		reportTable.getColumnModel().getColumn(1).setPreferredWidth(340);
		reportTable.getColumnModel().getColumn(2).setPreferredWidth(115);
		reportTable.getColumnModel().getColumn(3).setPreferredWidth(115);
		reportTable.getColumnModel().getColumn(4).setPreferredWidth(360);
		reportTable.getColumnModel().getColumn(5).setPreferredWidth(78);
		reportTable.getColumnModel().getColumn(6).setPreferredWidth(78);
		reportTable.getColumnModel().getColumn(7).setPreferredWidth(78);
		// reportTable.setAutoCreateRowSorter(true);

		TableRowSorter<javax.swing.table.TableModel> sorter = new TableRowSorter<javax.swing.table.TableModel>(
				reportTable.getModel());
		sorter.setSortable(0, true);
		sorter.setSortable(1, false);
		sorter.setSortable(2, true);
		sorter.setSortable(3, false);
		sorter.setSortable(4, false);
		sorter.setSortable(5, true);
		sorter.setSortable(6, true);
		sorter.setSortable(7, true);
		ArrayList<SortKey> keys = new ArrayList<SortKey>();
		keys.add(new SortKey(0, SortOrder.DESCENDING));

		sorter.setSortKeys(keys);
		sorter.toggleSortOrder(0);
		sorter.setSortsOnUpdates(true);
		reportTable.setRowSorter(sorter);

		reportTable.setDefaultRenderer(JTextArea.class, new TextAreaRenderer());
		reportTable.getTableHeader().setDefaultRenderer(new MultiLineHeaderRenderer());
		JScrollPane scrollPane = new JScrollPane(reportTable, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		reportTable.setPreferredScrollableViewportSize(new Dimension(1200, 500));
		reportTable.setFillsViewportHeight(true);
		viewReport.add(scrollPane);

		filter = new Filter(mainGraph);
		JPanel downPanel = new JPanel(new GridLayout(3, 3));
		JPanel downPanel2 = new JPanel(new GridLayout(1, 2));
		JPanel downPanel3 = new JPanel(new FlowLayout());
		JPanel downPanel4 = new JPanel(new GridLayout(1, 2));
		JPanel downPanel5 = new JPanel(new GridLayout(1, 2));

		JLabel labelName = new JLabel("Название статьи");
		JLabel labelTags = new JLabel("Теги");

		JLabel labelParentName = new JLabel("Статья родитель");
		JTextArea taParentName = new JTextArea(2, 17);

		JTextArea taName = new JTextArea(2, 17);
		JTextArea taFilter = new JTextArea(3, 20);
		JComboBox<String> comboBox = new JComboBox<>();

		JScrollPane scrollPane2 = new JScrollPane(taName, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JScrollPane scrollPane3 = new JScrollPane(taParentName, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		taFilter.setEditable(false);
		taName.setEditable(false);
		taParentName.setEditable(false);

		taFilter.setLineWrap(true);
		taFilter.setWrapStyleWord(true);
		taName.setLineWrap(true);
		taName.setWrapStyleWord(true);
		taParentName.setLineWrap(true);
		taParentName.setWrapStyleWord(true);

		downPanel4.add(labelName);
		downPanel4.add(scrollPane2);
		downPanel5.add(labelParentName);
		downPanel5.add(scrollPane3);

		downPanel.add(downPanel4);
		downPanel.add(taFilter);

		downPanel.add(btnGenerateByTags);
		downPanel.add(downPanel5);
		downPanel.add(btnClearFilter);
		downPanel.add(btnGenerateByLinks);
		downPanel2.add(labelTags);
		downPanel3.add(comboBox);
		downPanel2.add(downPanel3);
		downPanel.add(downPanel2);
		downPanel.add(btnAddTagToFilter);
		downPanel.add(btnGenerateExcel);

		viewReport.add(downPanel);

		viewReport.setVisible(true);

		reportTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					ArrayList<String> tags = mainGraph.get(reportTable.getSelectedRow()).getAtrTags();
					taName.setText(reportTable.getModel().getValueAt(reportTable.getSelectedRow(), 1).toString());
					taParentName.setText(mainGraph.get(reportTable.getSelectedRow()).getParentName());
					comboBox.removeAllItems();
					tags.forEach(t -> {
						comboBox.addItem(t);
					});
				}
			}
		});

		btnClearFilter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				taFilter.setText("");
				selectedTags = new HashSet<>();
			}
		});

		btnAddTagToFilter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String tmp = comboBox.getSelectedItem().toString();
				if (!selectedTags.contains(tmp)) {
					selectedTags.add(tmp);
					taFilter.append(comboBox.getSelectedItem().toString() + ", ");
				}

			}
		});

		btnGenerateByTags.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<Habrahabr> filteredGraph = filter.generateByTags(selectedTags);
				new ReportWindow(filteredGraph);
			}
		});

		btnGenerateExcel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				generateReport(mainGraph);
				viewReport.dispose();
			}
		});

		btnGenerateByLinks.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<Habrahabr> filteredGraph = new ArrayList<>();
				String parentName = mainGraph.get(reportTable.getSelectedRow()).getParentName();
				ArrayList<String> childs = mainGraph.get(reportTable.getSelectedRow()).getSimilarPubs();

				for (Habrahabr temp : mainGraph) {
					if (temp.getAtrName().equals(parentName)) {
						filteredGraph.add(temp);
						break;
					}
				}

				for (Habrahabr temp : mainGraph) {
					for (int i = 0; i < childs.size(); i++) {
						if (temp.getAtrName().equals(childs.get(i))) {
							filteredGraph.add(temp);
						}
					}
				}

				ReportWindow linksReport = new ReportWindow(filteredGraph);
				linksReport.setDisableButtons();
			}
		});
	}

	private void generateReport(ArrayList<Habrahabr> graph) {
		JFileChooser fc = new JFileChooser();
		FileNameExtensionFilter filterFileFormat = new FileNameExtensionFilter("*.xlsx", "*.xls", "*.*");
		fc.setFileFilter(filterFileFormat);
		fc.setSelectedFile(new File("excel_report"));
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					ExportReport exporter = new ExportReport(graph);
					if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
						exporter.exportToExcel(fc.getSelectedFile().getPath());
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}).start();
	}

	public void setDisableButtons() {
		btnAddTagToFilter.setEnabled(false);
		btnClearFilter.setEnabled(false);
		btnGenerateByTags.setEnabled(false);
		btnGenerateByLinks.setEnabled(false);
	}

	class TableModel extends AbstractTableModel { // модель таблицы

		private static final long serialVersionUID = 1L;
		private ArrayList<Habrahabr> mainGraph;

		private ArrayList<String[]> data;
		private ArrayList<Integer> id;

		public TableModel(ArrayList<Habrahabr> mainGraph) {
			this.mainGraph = mainGraph;
			data = new ArrayList<>();
			id = new ArrayList<>();
			for (int i = 0; i < this.mainGraph.size(); i++) {
				data.add(mainGraph.get(i).getData());
				id.add((i + 1));
			}
		}

		@Override
		public int getRowCount() {
			return mainGraph.size();
		}

		@Override
		public int getColumnCount() {
			return Constants.columnNames.length + 1;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			switch (columnIndex) {
			case 0:
			case 5:
			case 6:
			case 7:
				return Integer.class;
			case 2:
				return SimpleDateFormat.class;
			default:
				return JTextArea.class;
			}
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {

			switch (columnIndex) {
			case 0:
				return id.get(rowIndex);
			case 2:
				SimpleDateFormat parser = new SimpleDateFormat("dd MMMM yyyy 'в' HH:mm");
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd 'в' HH:mm");

				try {
					Date date = parser.parse(data.get(rowIndex)[columnIndex - 1]);
					return formatter.format(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			case 5:
				return Integer.parseInt(data.get(rowIndex)[columnIndex - 1]);
			case 6:
				String tmp = data.get(rowIndex)[columnIndex - 1].replace(",", ".");
				if (tmp.contains("k")) {
					tmp = tmp.split("k")[0];
					return (int) ((Double.parseDouble(tmp)) * 1000);
				} else {
					return (int) (Double.parseDouble(tmp));
				}

			case 7:
				return Integer.parseInt(data.get(rowIndex)[columnIndex - 1]);
			default:
				return data.get(rowIndex)[columnIndex - 1];
			}
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

		@Override
		public String getColumnName(int columnIndex) {
			switch (columnIndex) {
			case 0:
				return "ID";
			case 1:
				return Constants.columnNames[0];
			case 2:
				return Constants.columnNames[1];
			case 3:
				return Constants.columnNames[2];
			case 4:
				return Constants.columnNames[3];
			case 5:
				return Constants.columnNames[4];
			case 6:
				return Constants.columnNames[5];
			case 7:
				return Constants.columnNames[6];

			default:
				break;
			}
			return null;
		}

		public ArrayList<String> getTags(int index) {
			return mainGraph.get(index).getAtrTags();
		}
	}
}

class TextAreaRenderer extends JTextArea implements TableCellRenderer { // Для авто высоты ячеек.

	private static final long serialVersionUID = 2655365342727846419L;
	private final DefaultTableCellRenderer adaptee = new DefaultTableCellRenderer();

	public TextAreaRenderer() {
		setLineWrap(true);
		setWrapStyleWord(true);
	}

	int last_row = -1;

	public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus,
			int row, int column) {
		adaptee.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);
		setForeground(adaptee.getForeground());
		setBackground(adaptee.getBackground());
		setBorder(adaptee.getBorder());
		setFont(adaptee.getFont());
		setText(adaptee.getText());

		setText(obj == null ? "" : obj.toString());

		Rectangle rect = table.getCellRect(row, column, true);
		this.setSize(rect.getSize());
		int height_wanted = (int) getPreferredSize().getHeight();
		if ((height_wanted > table.getRowHeight(row) | row != last_row) &

				height_wanted > table.getRowHeight())
			table.setRowHeight(row, height_wanted);
		last_row = row;
		return this;
	}
}

class MultiLineHeaderRenderer extends JTextArea implements TableCellRenderer { // для заголовка

	private static final long serialVersionUID = -8393904854037898292L;

	public MultiLineHeaderRenderer() {
		setAlignmentY(JLabel.CENTER);
		setLineWrap(true);
		setWrapStyleWord(true);
		setBackground(Color.gray);
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK),
				BorderFactory.createEmptyBorder(3, 3, 3, 3)));
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		setFont(table.getFont());
		String str = (value == null) ? "" : value.toString();
		setText(str);
		int columnWidth = getColumnWidth();
		setRows(str.length() / columnWidth);
		return this;
	}
}