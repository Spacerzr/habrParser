package ru.gazprom_inform.dgzorin.logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SiteParser {
	private String link = Constants.graphVertexLink;
	private Habrahabr oneVertex;
	private Random random = new Random();
	private Logger logger = new Logger();
	private int graphSize = 500;
	private String parentName = "Корневой каталог";

	// проверка ссылки
	public boolean checkLink(String link) {
		try {
			String tmp = link.substring(0, 20);
			if (!tmp.equals(Constants.habrSitelink)) {
				logger.sendMsgToLog("Ссылка недействительна. Необходимо указать сайт " + Constants.habrSitelink);
				return false;
			}
			Document doc = Jsoup.connect(link).get();
			logger.sendMsgToLog("Ссылка проверена.\n");
			return true;
		} catch (Exception e) {
			logger.sendMsgToLog("Ссылка недействительна. Проверьте правильность написания ссылки.");
			return false;
		}
	}

	// заполнение графа
	public ArrayList<Habrahabr> parseHabrahabr() {
		long startTime = System.currentTimeMillis();

		ArrayList<Habrahabr> graph = new ArrayList<>();
		ArrayList<String> tempLinks = new ArrayList<>();

		for (int i = 0; i < graphSize; i++) {
			oneVertex = parseOnePage(link);
			if (oneVertex == null) {
				i--;
				link = tempLinks.get(random.nextInt(3));
				continue;

			}
			graph.add(oneVertex);

			tempLinks = oneVertex.getSimilarPubsLinks();

			link = tempLinks.get(random.nextInt(3));
		}
		long executionTime = (System.currentTimeMillis() - startTime) / 1000;
		logger.sendMsgToLog("Граф успешно сформирован за " + executionTime + " сек.");

		return graph;
	}

	// парсинг страницы
	private Habrahabr parseOnePage(String link) {
		Document doc;
		Elements elms;
		Element elm;

		String atrName;
		String atrDate;
		ArrayList<String> atrTags = new ArrayList<>();
		HashSet<String> allTags = new HashSet<>();
		ArrayList<String> similarPubs = new ArrayList<>();
		ArrayList<String> similarPubsLinks = new ArrayList<>();
		int atrRating;
		String atrViewsCount;
		String atrSaveCount;

		try {
			// получение страницы
			doc = Jsoup.connect(link).get();

			// получение темы статьи
			String[] tmpTitle = doc.title().split("/");
			atrName = tmpTitle[0].substring(0, tmpTitle[0].length() - 1);

			// получение даты публикации
			elms = doc.getElementsByAttributeValue(Constants.strClass, Constants.searchDate);

			atrDate = elms.get(0).child(1).text();

			// получение тегов (меток)
			elms = doc.getElementsByAttributeValue(Constants.strClass, Constants.searchTags);

			elms.forEach(temp -> {
				String tempTag = temp.child(0).text();
				atrTags.add(tempTag);
				allTags.add(tempTag);
			});

			// получение похожих публикаций
			elms = doc.getElementsByAttributeValue(Constants.strClass, Constants.searchSimilarPubs);
			try {
				elm = elms.get(0);
				for (int i = 0; i < 3; i++) {
					similarPubs.add(elm.child(i).child(1).text());
					similarPubsLinks.add(Constants.habrSitelink + elm.child(i).child(1).attr("href"));
				}
			} catch (IndexOutOfBoundsException ex) {
				return null;
			}

			// получение количества рейтинга, количества просмотров и количества
			// сохранений(добавлений в закладки)
			elms = doc.getElementsByAttributeValue(Constants.strClass, Constants.searchRatings);
			String atrRatingTmp;

			elm = elms.get(0);
			// рейтинг
			atrRatingTmp = elm.child(0).text();

			char tmpCh = atrRatingTmp.charAt(0);
			if (tmpCh == '+') {
				atrRatingTmp = atrRatingTmp.substring(1, atrRatingTmp.length());
			}
			atrRating = Integer.parseInt(atrRatingTmp);

			// количество просмотров
			atrViewsCount = elm.child(2).text();

			// количество сохранений
			atrSaveCount = elm.child(1).text();

			// Возврат новой вершины графа
			Habrahabr vertex = new Habrahabr(atrName, atrDate, atrTags, similarPubs, similarPubsLinks, atrRating,
					atrViewsCount, atrSaveCount, parentName);
			parentName = atrName;

			return vertex;

		} catch (java.net.ConnectException e2) {
			e2.printStackTrace();
			logger.sendMsgToLog("Произошла ошибка : Connection timed out: connect.");
			return null;
		} catch (org.jsoup.HttpStatusException e3) {
			e3.printStackTrace();
			logger.sendMsgToLog("Произошла ошибка : HTTP error fetching URL. Status=503.");
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			logger.sendMsgToLog("Произошла ошибка формирования графа. Пожалуйста, попробуйте еще раз.");
			return null;
		}
	}

	public void setGraphSize(int graphSize) {
		this.graphSize = graphSize;
	}

}
