package ru.gazprom_inform.dgzorin.logic;

public class Constants {
	public static final String habrSitelink = "https://habrahabr.ru"; //

	public static final String graphVertexLink = "https://habrahabr.ru/post/43293/"; // исходная вершина графа

	public static final String additionalLink = "https://habrahabr.ru/post/253787/";

	public static final String strClass = "class";
	public static final String searchDate = "post__meta";
	public static final String searchTags = "inline-list__item inline-list__item_tag";
	public static final String searchSimilarPubs = "content-list";
	public static final String searchRating = "voting-wjt__counter voting-wjt__counter_positive  js-score";
	public static final String searchRatings = "post-stats post-stats_post js-user_";

	public static final String[] columnNames = { "Тема Статьи", "Дата публикации", "Теги", "Похожие публикации",
			"Рейтинг", "Число просмотров", "Число сохранений" };

}
