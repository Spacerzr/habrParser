package ru.gazprom_inform.dgzorin.logic;

import java.util.ArrayList;

/*
 * Вершины графа сайта habrahabr
 */

public class Habrahabr {
	// основные атрибуты графа
	private String atrName;
	private String atrDate;
	private ArrayList<String> atrTags;
	private ArrayList<String> similarPubs;
	private int atrRating;
	private String atrViewsCount;
	private String atrSaveCount;

	// дополнительные атрибуты (hidden)
	private ArrayList<String> similarPubsLinks;
	private String parentName;

	public Habrahabr(String atrName, String atrDate, ArrayList<String> atrTags, ArrayList<String> similarPubs,
			ArrayList<String> similarPubsLinks, int atrRating, String atrViewsCount, String atrSaveCount,
			String parentName) {
		this.atrName = atrName;
		this.atrDate = atrDate;
		this.atrTags = atrTags;
		this.similarPubs = similarPubs;
		this.similarPubsLinks = similarPubsLinks;
		this.atrRating = atrRating;
		this.atrViewsCount = atrViewsCount;
		this.atrSaveCount = atrSaveCount;
		this.parentName = parentName;
	}

	public String getAtrName() {
		return atrName;
	}

	public void setAtrName(String atrName) {
		this.atrName = atrName;
	}

	public String getAtrDate() {
		return atrDate;
	}

	public void setAtrDate(String atrDate) {
		this.atrDate = atrDate;
	}

	public ArrayList<String> getAtrTags() {
		return atrTags;
	}

	public void setAtrTags(ArrayList<String> atrTags) {
		this.atrTags = atrTags;
	}

	public ArrayList<String> getSimilarPubs() {
		return similarPubs;
	}

	public void setSimilarPubs(ArrayList<String> similarPubs) {
		this.similarPubs = similarPubs;
	}

	public ArrayList<String> getSimilarPubsLinks() {
		return similarPubsLinks;
	}

	public void setSimilarPubsLinks(ArrayList<String> similarPubsLinks) {
		this.similarPubsLinks = similarPubsLinks;
	}

	public int getAtrRating() {
		return atrRating;
	}

	public void setAtrRating(int atrRating) {
		this.atrRating = atrRating;
	}

	public String getAtrViewsCount() {
		return atrViewsCount;
	}

	public void setAtrViewsCount(String atrViewsCount) {
		this.atrViewsCount = atrViewsCount;
	}

	public String getAtrSaveCount() {
		return atrSaveCount;
	}

	public void setAtrSaveCount(String atrSaveCount) {
		this.atrSaveCount = atrSaveCount;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String[] getData() {
		String tags = "";
		String pubs = "";

		for (String tmp : atrTags) {
			tags += tmp + "\r\n";
		}
		for (String tmp : similarPubs) {
			pubs += tmp + "\r\n";
		}

		String[] data = { atrName, atrDate, tags, pubs, "" + atrRating, atrViewsCount, atrSaveCount };

		return data;
	}

}