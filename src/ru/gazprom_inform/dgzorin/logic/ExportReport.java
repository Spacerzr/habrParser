package ru.gazprom_inform.dgzorin.logic;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExportReport {
	ArrayList<Habrahabr> graph = new ArrayList<>();
	private Logger logger = new Logger();

	public ExportReport(ArrayList<Habrahabr> graph) {
		this.graph = graph;
	}

	public void exportToExcel(String exportPath) throws FileNotFoundException, IOException {
		long startTime = System.currentTimeMillis();

		Workbook book = new HSSFWorkbook();
		Sheet sheet = book.createSheet("report");
		CellStyle header = book.createCellStyle();
		Font fontBold = book.createFont();
		fontBold.setBold(true);
		fontBold.setFontHeight((short) 300);
		header.setWrapText(true);
		header.setFont(fontBold);

		CellStyle dataStyle = book.createCellStyle();
		Font fontData = book.createFont();
		dataStyle.setWrapText(true);
		dataStyle.setFont(fontData);

		Row row;
		Cell name;
		Cell date;
		Cell tags;
		Cell similarPubs;
		Cell rating;
		Cell viewsCount;
		Cell saveCount;

		row = sheet.createRow(0);

		name = row.createCell(0);
		date = row.createCell(1);
		tags = row.createCell(2);
		similarPubs = row.createCell(3);
		rating = row.createCell(4);
		viewsCount = row.createCell(5);
		saveCount = row.createCell(6);

		name.setCellStyle(header);
		date.setCellStyle(header);
		tags.setCellStyle(header);
		similarPubs.setCellStyle(header);
		rating.setCellStyle(header);
		viewsCount.setCellStyle(header);
		saveCount.setCellStyle(header);

		name.setCellValue(Constants.columnNames[0]);
		date.setCellValue(Constants.columnNames[1]);
		tags.setCellValue(Constants.columnNames[2]);
		similarPubs.setCellValue(Constants.columnNames[3]);
		rating.setCellValue(Constants.columnNames[4]);
		viewsCount.setCellValue(Constants.columnNames[5]);
		saveCount.setCellValue(Constants.columnNames[6]);

		// =========================================================

		for (int i = 0; i < graph.size(); i++) {

			Habrahabr temp = graph.get(i);

			row = sheet.createRow(i + 1);
			name = row.createCell(0);
			date = row.createCell(1);
			tags = row.createCell(2);
			similarPubs = row.createCell(3);
			rating = row.createCell(4);
			viewsCount = row.createCell(5);
			saveCount = row.createCell(6);

			name.setCellStyle(dataStyle);
			date.setCellStyle(dataStyle);
			tags.setCellStyle(dataStyle);
			similarPubs.setCellStyle(dataStyle);
			rating.setCellStyle(dataStyle);
			viewsCount.setCellStyle(dataStyle);
			saveCount.setCellStyle(dataStyle);

			name.setCellValue(temp.getAtrName());
			date.setCellValue(temp.getAtrDate());

			ArrayList<String> tagsTemp = temp.getAtrTags();
			String tagsTempResult = "";
			for (String tmp : tagsTemp) {
				tagsTempResult += tmp + "\n";
			}
			tags.setCellValue(tagsTempResult);

			ArrayList<String> similarPubsTemp = temp.getSimilarPubs();
			String similarPubsTempResult = "";
			for (String tmp : similarPubsTemp) {
				similarPubsTempResult += tmp + "\n";
			}
			similarPubs.setCellValue(similarPubsTempResult);
			rating.setCellValue("" + temp.getAtrRating());
			viewsCount.setCellValue(temp.getAtrViewsCount());
			saveCount.setCellValue(temp.getAtrSaveCount());
		}

		// Меняем размер столбца
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		sheet.autoSizeColumn(6);

		// Записываем всё в файл
		book.write(new FileOutputStream(exportPath + ".xls", false));
		book.close();
		long executionTime = (System.currentTimeMillis() - startTime) / 1000;
		logger.sendMsgToLog("Выгрузка отчета завершена за " + executionTime + "сек.\n" + exportPath + ".xls.");
	}
}
