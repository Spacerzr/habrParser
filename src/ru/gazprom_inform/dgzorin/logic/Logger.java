package ru.gazprom_inform.dgzorin.logic;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.gazprom_inform.dgzorin.appgui.MainAppWindow;

public class Logger {
	private static SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy 'at' HH.mm ': '");

	public void sendMsgToLog(String msg) {
		MainAppWindow.log.append(date.format(new Date()) + msg + "\n");
	}
}
