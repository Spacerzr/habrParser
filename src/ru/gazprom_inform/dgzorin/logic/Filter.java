package ru.gazprom_inform.dgzorin.logic;

import java.util.ArrayList;
import java.util.HashSet;

public class Filter {
	private ArrayList<Habrahabr> mainGraph = new ArrayList<>();
	private HashSet<String> allTags = new HashSet<>();

	public Filter(ArrayList<Habrahabr> mainGraph) {
		this.mainGraph = mainGraph;
		this.allTags = generateAllTags(mainGraph);
	}

	public ArrayList<Habrahabr> generateByTags(HashSet<String> selectedTags) {
		ArrayList<Habrahabr> filteredGraph = new ArrayList<>();
		Habrahabr tempGraph;
		ArrayList<String> tempTags = new ArrayList<>();

		for (int i = 0; i < mainGraph.size(); i++) {
			tempGraph = mainGraph.get(i);

			for (String tempTag : tempGraph.getAtrTags()) {
				tempTags.add(tempTag.toLowerCase());
			}

			for (String s : selectedTags) {
				if (tempTags.contains(s)) {
					filteredGraph.add(tempGraph);
					break;
				}
			}
			tempTags = new ArrayList<>();
		}

		return filteredGraph;
	}

	private HashSet<String> generateAllTags(ArrayList<Habrahabr> graph) {
		HashSet<String> allTags = new HashSet<>();
		graph.forEach(tempGraph -> {
			tempGraph.getAtrTags().forEach(tempTag -> {
				allTags.add(tempTag.toLowerCase());
			});
		});
		return allTags;
	}

	public HashSet<String> getAllTags() {
		return allTags;
	}
}
